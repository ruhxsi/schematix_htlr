﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchematiX.Helper
{
    public class Helper
    {
        [Flags]
        public enum Direction
        {
            Unspec = 0,
            East = 1,
            North = 2,
            South = 4,
            West = 8
        }
    }
}
