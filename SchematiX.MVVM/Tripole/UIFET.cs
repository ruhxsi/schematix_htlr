﻿using System.Windows.Shapes;
using System.Windows.Media;
using SchematiX.Model;
using System.Windows;
using static SchematiX.Helper.Helper;


namespace SchematiX.MVVM
{
    public class UIFET : UIPart
    {
        public override Path Path
        {
            get
            {
                Path p = base.Path;

                GeometryGroup gg = new GeometryGroup();
                Brush brush = new SolidColorBrush(Colors.Black);

                switch (Part.Direction)
                {

                    case Direction.East:
                        #region EW
                        //Gate

                        //Horizontaler Strich
                        gg.Children.Add(new LineGeometry(Part.Plugs[0].Location, new Point(Part.Plugs[0].Location.X - Part.Size.Height / 2, Part.Plugs[0].Location.Y)));
                        //Vertikaler Strich
                        gg.Children.Add(new LineGeometry(new Point(Part.Plugs[0].Location.X - Part.Size.Height / 2, Part.Plugs[0].Location.Y),
                            new Point(Part.Plugs[0].Location.X - Part.Size.Height / 2, Part.Plugs[0].Location.Y - Part.Size.Height / 2)));


                        //Source

                        //langer Vertikale Strich
                        gg.Children.Add(new LineGeometry(Part.Plugs[1].Location, new Point(Part.Plugs[1].Location.X, Part.Plugs[1].Location.Y - Part.Size.Height / 6 * 4)));

                        //kleiner horizontale Strich (nicht der Pfeil)
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X, Part.Plugs[1].Location.Y - Part.Size.Height / 2),
                            new Point(Part.Plugs[1].Location.X + Part.Size.Width / 5, Part.Plugs[1].Location.Y - Part.Size.Height / 2)));
                        //kleiner vertikale Strich
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X + Part.Size.Width / 5, Part.Plugs[1].Location.Y - Part.Size.Height / 18 * 10),
                            new Point(Part.Plugs[1].Location.X + Part.Size.Width / 5, Part.Plugs[1].Location.Y - Part.Size.Height / 18 * 8)));

                        //kleiner horizontale Strich(Pfeil)
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X, Part.Plugs[1].Location.Y - Part.Size.Height / 6 * 4),
                            new Point(Part.Plugs[1].Location.X + Part.Size.Width / 5, Part.Plugs[1].Location.Y - Part.Size.Height / 6 * 4)));
                        //Pfeil
                        gg.Children.Add(new LineGeometry(new Point(Part.Plugs[1].Location.X + Part.Size.Width / 5, Part.Plugs[1].Location.Y - Part.Size.Height / 6 * 4), new Point(Part.Plugs[1].Location.X + Part.Size.Height / 10, Part.Plugs[1].Location.Y - Part.Size.Height / 18 * 13)));
                        gg.Children.Add(new LineGeometry(new Point(Part.Plugs[1].Location.X + Part.Size.Width / 5, Part.Plugs[1].Location.Y - Part.Size.Height / 6 * 4), new Point(Part.Plugs[1].Location.X + Part.Size.Height / 10, Part.Plugs[1].Location.Y - Part.Size.Height / 18 * 11)));


                        //Drain

                        //langer vertikaler Strich
                        gg.Children.Add(new LineGeometry(new Point(Part.Plugs[2].Location.X, Part.Plugs[2].Location.Y),
                            new Point(Part.Plugs[2].Location.X, Part.Plugs[2].Location.Y + Part.Size.Height / 18 * 3)));
                        //horizontaler Strich
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X, Part.Plugs[1].Location.Y - Part.Size.Height / 6 * 5),
                           new Point(Part.Plugs[1].Location.X + Part.Size.Width / 5, Part.Plugs[1].Location.Y - Part.Size.Height / 6 * 5)));
                        //kleiner vertikaler Strich
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X + Part.Size.Width / 5, Part.Plugs[1].Location.Y - Part.Size.Height / 18 * 14),
                            new Point(Part.Plugs[1].Location.X + Part.Size.Width / 5, Part.Plugs[1].Location.Y - Part.Size.Height / 18 * 16)));



                        p.Data = gg;



                        #endregion
                        break;

                    case Direction.South:
                        #region SN



                        //Gate

                        //Horizontaler Strich
                        gg.Children.Add(new LineGeometry(Part.Plugs[0].Location, new Point(Part.Plugs[0].Location.X, Part.Plugs[0].Location.Y - Part.Size.Height / 10 * 6)));
                        //Vertikaler Strich
                        gg.Children.Add(new LineGeometry(new Point(Part.Plugs[0].Location.X, Part.Plugs[0].Location.Y - Part.Size.Height / 10 * 6),
                            new Point(Part.Plugs[0].Location.X + Part.Size.Width / 2, Part.Plugs[0].Location.Y - Part.Size.Height / 10 * 6)));


                        //Source

                        //langer Horizontaler Strich
                        gg.Children.Add(new LineGeometry(Part.Plugs[1].Location, new Point(Part.Plugs[1].Location.X + Part.Size.Width / 6 * 4, Part.Plugs[1].Location.Y)));
                        //kleiner vertikaler Strich (nicht der Pfeil)
                        gg.Children.Add(new LineGeometry(
                              new Point(Part.Plugs[1].Location.X + Part.Size.Width / 2, Part.Plugs[1].Location.Y),
                              new Point(Part.Plugs[1].Location.X + Part.Size.Width / 2, Part.Plugs[1].Location.Y + Part.Size.Width / 5)));
                        //kleiner Horizontaler Strich
                        gg.Children.Add(new LineGeometry(
                              new Point(Part.Plugs[1].Location.X + Part.Size.Width / 18 * 10, Part.Plugs[1].Location.Y + Part.Size.Width / 5),
                              new Point(Part.Plugs[1].Location.X + Part.Size.Width / 18 * 8, Part.Plugs[1].Location.Y + Part.Size.Width / 5)));
                        //kleiner vertikaler Strich(Pfeil)
                        gg.Children.Add(new LineGeometry(
                              new Point(Part.Plugs[1].Location.X + Part.Size.Width / 6 * 4, Part.Plugs[1].Location.Y),
                              new Point(Part.Plugs[1].Location.X + Part.Size.Width / 6 * 4, Part.Plugs[1].Location.Y + Part.Size.Width / 5)));
                        //Pfeil
                        gg.Children.Add(new LineGeometry(new Point(Part.Plugs[1].Location.X + Part.Size.Width / 6 * 4, Part.Plugs[1].Location.Y + Part.Size.Width / 5), new Point(Part.Plugs[1].Location.X + Part.Size.Width / 18 * 13, Part.Plugs[1].Location.Y + Part.Size.Height / 10)));
                        gg.Children.Add(new LineGeometry(new Point(Part.Plugs[1].Location.X + Part.Size.Width / 6 * 4, Part.Plugs[1].Location.Y + Part.Size.Width / 5), new Point(Part.Plugs[1].Location.X + Part.Size.Width / 18 * 11, Part.Plugs[1].Location.Y + Part.Size.Height / 10)));


                        //Drain

                        //langer horizontaler Strich
                        gg.Children.Add(new LineGeometry(new Point(Part.Plugs[2].Location.X, Part.Plugs[2].Location.Y),
                          new Point(Part.Plugs[2].Location.X - Part.Size.Width / 6, Part.Plugs[2].Location.Y)));
                        //vertikaler Strich
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X + Part.Size.Width / 6 * 5, Part.Plugs[1].Location.Y),
                            new Point(Part.Plugs[1].Location.X + Part.Size.Width / 6 * 5, Part.Plugs[1].Location.Y + Part.Size.Width / 5)));
                        //kleiner Horizontaler Strich
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X + Part.Size.Width / 18 * 14, Part.Plugs[1].Location.Y + Part.Size.Width / 5),
                            new Point(Part.Plugs[1].Location.X + Part.Size.Width / 18 * 16, Part.Plugs[1].Location.Y + Part.Size.Width / 5)));


                        p.Data = gg;


                        #endregion
                        break;

                    case Direction.West:
                        #region WE

                        //Gate

                        //Horizontaler Strich
                        gg.Children.Add(new LineGeometry(Part.Plugs[0].Location, new Point(Part.Plugs[0].Location.X + Part.Size.Height / 2, Part.Plugs[0].Location.Y)));
                        //Vertikaler Strich
                        gg.Children.Add(new LineGeometry(new Point(Part.Plugs[0].Location.X + Part.Size.Height / 2, Part.Plugs[0].Location.Y),
                            new Point(Part.Plugs[0].Location.X + Part.Size.Height / 2, Part.Plugs[0].Location.Y + Part.Size.Height / 2)));



                        //Source

                        //langer Vertikale Strich
                        gg.Children.Add(new LineGeometry(Part.Plugs[1].Location, new Point(Part.Plugs[1].Location.X, Part.Plugs[1].Location.Y + Part.Size.Height / 6 * 4)));
                        //kleiner horizontale Strich (nicht der Pfeil)
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X, Part.Plugs[1].Location.Y + Part.Size.Height / 2),
                            new Point(Part.Plugs[1].Location.X - Part.Size.Width / 5, Part.Plugs[1].Location.Y + Part.Size.Height / 2)));
                        //kleiner vertikale Strich
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X - Part.Size.Width / 5, Part.Plugs[1].Location.Y + Part.Size.Height / 18 * 10),
                            new Point(Part.Plugs[1].Location.X - Part.Size.Width / 5, Part.Plugs[1].Location.Y + Part.Size.Height / 18 * 8)));
                        //kleiner horizontale Strich(Pfeil)
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X, Part.Plugs[1].Location.Y + Part.Size.Height / 6 * 4),
                            new Point(Part.Plugs[1].Location.X - Part.Size.Width / 5, Part.Plugs[1].Location.Y + Part.Size.Height / 6 * 4)));
                        //Pfeil
                        gg.Children.Add(new LineGeometry(new Point(Part.Plugs[1].Location.X - Part.Size.Width / 5, Part.Plugs[1].Location.Y + Part.Size.Height / 6 * 4), new Point(Part.Plugs[1].Location.X - Part.Size.Height / 10, Part.Plugs[1].Location.Y + Part.Size.Height / 18 * 13)));
                        gg.Children.Add(new LineGeometry(new Point(Part.Plugs[1].Location.X - Part.Size.Width / 5, Part.Plugs[1].Location.Y + Part.Size.Height / 6 * 4), new Point(Part.Plugs[1].Location.X - Part.Size.Height / 10, Part.Plugs[1].Location.Y + Part.Size.Height / 18 * 11)));



                        //Drain

                        //langer vertikaler Strich
                        gg.Children.Add(new LineGeometry(new Point(Part.Plugs[2].Location.X, Part.Plugs[2].Location.Y),
                            new Point(Part.Plugs[2].Location.X, Part.Plugs[2].Location.Y - Part.Size.Height / 18 * 3)));
                        //horizontaler Strich
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X, Part.Plugs[1].Location.Y + Part.Size.Height / 6 * 5),
                           new Point(Part.Plugs[1].Location.X - Part.Size.Width / 5, Part.Plugs[1].Location.Y + Part.Size.Height / 6 * 5)));
                        //kleiner vertikaler Strich
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X - Part.Size.Width / 5, Part.Plugs[1].Location.Y + Part.Size.Height / 18 * 14),
                            new Point(Part.Plugs[1].Location.X - Part.Size.Width / 5, Part.Plugs[1].Location.Y + Part.Size.Height / 18 * 16)));





                        p.Data = gg;

                        #endregion
                        break;
                    case Direction.North:
                        #region NS



                        //Gate

                        //Horizontaler Strich
                        gg.Children.Add(new LineGeometry(Part.Plugs[0].Location, new Point(Part.Plugs[0].Location.X, Part.Plugs[0].Location.Y + Part.Size.Height / 10 * 6)));
                        //Vertikaler Strich
                        gg.Children.Add(new LineGeometry(new Point(Part.Plugs[0].Location.X, Part.Plugs[0].Location.Y + Part.Size.Height / 10 * 6),
                            new Point(Part.Plugs[0].Location.X - Part.Size.Width / 2, Part.Plugs[0].Location.Y + Part.Size.Height / 10 * 6)));


                        //Source

                        //langer Horizontaler Strich
                        gg.Children.Add(new LineGeometry(Part.Plugs[1].Location, new Point(Part.Plugs[1].Location.X - Part.Size.Width / 6 * 4, Part.Plugs[1].Location.Y)));
                        //kleiner vertikaler Strich (nicht der Pfeil)
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X - Part.Size.Width / 2, Part.Plugs[1].Location.Y),
                            new Point(Part.Plugs[1].Location.X - Part.Size.Width / 2, Part.Plugs[1].Location.Y - Part.Size.Width / 5)));
                        //kleiner Horizontaler Strich
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X - Part.Size.Width / 18 * 10, Part.Plugs[1].Location.Y - Part.Size.Width / 5),
                            new Point(Part.Plugs[1].Location.X - Part.Size.Width / 18 * 8, Part.Plugs[1].Location.Y - Part.Size.Width / 5)));
                        //kleiner vertikaler Strich(Pfeil)
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X - Part.Size.Width / 6 * 4, Part.Plugs[1].Location.Y),
                            new Point(Part.Plugs[1].Location.X - Part.Size.Width / 6 * 4, Part.Plugs[1].Location.Y - Part.Size.Width / 5)));
                        //Pfeil
                        gg.Children.Add(new LineGeometry(new Point(Part.Plugs[1].Location.X - Part.Size.Width / 6 * 4, Part.Plugs[1].Location.Y - Part.Size.Width / 5), new Point(Part.Plugs[1].Location.X - Part.Size.Width / 18 * 13, Part.Plugs[1].Location.Y - Part.Size.Height / 10)));
                        gg.Children.Add(new LineGeometry(new Point(Part.Plugs[1].Location.X - Part.Size.Width / 6 * 4, Part.Plugs[1].Location.Y - Part.Size.Width / 5), new Point(Part.Plugs[1].Location.X - Part.Size.Width / 18 * 11, Part.Plugs[1].Location.Y - Part.Size.Height / 10)));



                        //Drain
                        gg.Children.Add(new LineGeometry(new Point(Part.Plugs[2].Location.X, Part.Plugs[2].Location.Y),
                          new Point(Part.Plugs[2].Location.X + Part.Size.Width / 6, Part.Plugs[2].Location.Y)));
                        //langer horizontaler Strich
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X - Part.Size.Width / 6 * 5, Part.Plugs[1].Location.Y),
                            new Point(Part.Plugs[1].Location.X - Part.Size.Width / 6 * 5, Part.Plugs[1].Location.Y - Part.Size.Width / 5)));
                        //vertikaler Strich
                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Plugs[1].Location.X - Part.Size.Width / 18 * 14, Part.Plugs[1].Location.Y - Part.Size.Width / 5),
                            new Point(Part.Plugs[1].Location.X - Part.Size.Width / 18 * 16, Part.Plugs[1].Location.Y - Part.Size.Width / 5)));
                        //kleiner Horizontaler Strich
                        p.Data = gg;



                        #endregion
                        break;
                    default:
                        break;

                }


                return p;
            }
        }
    }
}
