﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using static SchematiX.Helper.Helper;

namespace SchematiX.MVVM
{
    public class UICurrent:UIPart
    {
        public override Path Path
        {
            get
            {
                Path p = base.Path;
                GeometryGroup gg = new GeometryGroup();
                gg.FillRule = FillRule.Nonzero;

                switch (Part.Direction)
                {
                    case Helper.Helper.Direction.Unspec:
                        break;
                    case Helper.Helper.Direction.West:
                        gg.Children = new GeometryCollection
                {
                    new LineGeometry(new Point(Part.Location.X, Part.Location.Y + 25), new Point(Part.Location.X + 25, Part.Location.Y + 25)),
                    new LineGeometry(new Point(Part.Location.X+75, Part.Location.Y + 25), new Point(Part.Location.X + 100, Part.Location.Y + 25)),
                    new LineGeometry(new Point(Part.Location.X+50, Part.Location.Y ), new Point(Part.Location.X + 50, Part.Location.Y + 50)),
                    new EllipseGeometry(new Point(Part.Location.X + 50, Part.Location.Y + 25), 25, 25),
                  
                    new LineGeometry(new Point(Part.Location.X + 10 , Part.Location.Y + 25), new Point(Part.Location.X + 20 , Part.Location.Y+20)),
                    new LineGeometry(new Point(Part.Location.X + 10, Part.Location.Y + 25), new Point(Part.Location.X + 20,Part.Location.Y+30)),

                };
                        break;
                    case Helper.Helper.Direction.East:
                        gg.Children = new GeometryCollection
                {
                    new LineGeometry(new Point(Part.Location.X, Part.Location.Y + 25), new Point(Part.Location.X + 25, Part.Location.Y + 25)),
                    new LineGeometry(new Point(Part.Location.X+75, Part.Location.Y + 25), new Point(Part.Location.X + 100, Part.Location.Y + 25)),
                    new LineGeometry(new Point(Part.Location.X+50, Part.Location.Y ), new Point(Part.Location.X + 50, Part.Location.Y + 50)),
                    new EllipseGeometry(new Point(Part.Location.X + 50, Part.Location.Y + 25), 25, 25),

                    new LineGeometry(new Point(Part.Location.X + 90 , Part.Location.Y + 25), new Point(Part.Location.X + 80 , Part.Location.Y+20)),
                    new LineGeometry(new Point(Part.Location.X + 90, Part.Location.Y + 25), new Point(Part.Location.X + 80,Part.Location.Y+30)),
                };
                        break;
                    case Helper.Helper.Direction.North:
                        gg.Children = new GeometryCollection
                    {
                       new LineGeometry(new Point(Part.Location.X+25, Part.Location.Y ), new Point(Part.Location.X +25, Part.Location.Y + 25)),
                       new LineGeometry(new Point(Part.Location.X+25, Part.Location.Y+75 ), new Point(Part.Location.X +25, Part.Location.Y + 100)),
                        new LineGeometry(new Point(Part.Location.X, Part.Location.Y+50), new Point(Part.Location.X +50, Part.Location.Y + 50)),
                        new EllipseGeometry(new Point(Part.Location.X +25, Part.Location.Y + 50), 25, 25),

                  
                    new LineGeometry(new Point(Part.Location.X + 25 , Part.Location.Y + 10), new Point(Part.Location.X + 20 , Part.Location.Y+20)),
                    new LineGeometry(new Point(Part.Location.X +25, Part.Location.Y + 10), new Point(Part.Location.X + 30,Part.Location.Y+20)),

                    };
                        break;
                    case Helper.Helper.Direction.South:
                        gg.Children = new GeometryCollection
                    {
                      new LineGeometry(new Point(Part.Location.X+25, Part.Location.Y ), new Point(Part.Location.X +25, Part.Location.Y + 25)),
                       new LineGeometry(new Point(Part.Location.X+25, Part.Location.Y+75 ), new Point(Part.Location.X +25, Part.Location.Y + 100)),
                        new LineGeometry(new Point(Part.Location.X, Part.Location.Y+50), new Point(Part.Location.X +50, Part.Location.Y + 50)),
                        new EllipseGeometry(new Point(Part.Location.X +25, Part.Location.Y + 50), 25, 25),


                    new LineGeometry(new Point(Part.Location.X + 25 , Part.Location.Y + 90), new Point(Part.Location.X + 20 , Part.Location.Y+80)),
                    new LineGeometry(new Point(Part.Location.X +25, Part.Location.Y + 90), new Point(Part.Location.X + 30,Part.Location.Y+80)),


                    };
                        break;
                    default:
                        break;
                }

                p.Data = gg;
                return p;
            }
        }  
    }
}
