﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchematiX.MVVM
{
    public class UIInductor : UIPart
    {
        public override Path Path
        {
            get
            {
                Path p = base.Path;
                p.Fill = Stroke;

                GeometryGroup gg = new GeometryGroup();
                gg.FillRule = FillRule.Nonzero;

                switch (Part.Direction)
                {
                    case Helper.Helper.Direction.Unspec:
                        break;
                    case Helper.Helper.Direction.East:
                        RectangleGeometry rect = new RectangleGeometry(new Rect(Part.Location.X + 25, Part.Location.Y + 12.5, 50, 25));
                        gg.Children.Add(rect);

                        gg.Children.Add(new LineGeometry(new Point(Part.Location.X, Part.Location.Y + 25), new Point(Part.Location.X + 25, Part.Location.Y + 25)));
                        gg.Children.Add(new LineGeometry(new Point(Part.Location.X + 75, Part.Location.Y + 25), new Point(Part.Location.X + 100, Part.Location.Y + 25)));
                        break;
                    case Helper.Helper.Direction.South:
                        RectangleGeometry rectan = new RectangleGeometry(new Rect(Part.Location.X + 12.5, Part.Location.Y + 25, 25, 50));
                        gg.Children.Add(rectan);

                        gg.Children.Add(new LineGeometry(new Point(Part.Location.X + 25, Part.Location.Y), new Point(Part.Location.X + 25, Part.Location.Y + 25)));
                        gg.Children.Add(new LineGeometry(new Point(Part.Location.X + 25, Part.Location.Y + 75), new Point(Part.Location.X + 25, Part.Location.Y + 100)));
                        break;
                    case Helper.Helper.Direction.West:
                        RectangleGeometry recta = new RectangleGeometry(new Rect(Part.Location.X + 25, Part.Location.Y + 12.5, 50, 25));
                        gg.Children.Add(recta);

                        gg.Children.Add(new LineGeometry(new Point(Part.Location.X, Part.Location.Y + 25), new Point(Part.Location.X + 25, Part.Location.Y + 25)));
                        gg.Children.Add(new LineGeometry(new Point(Part.Location.X + 75, Part.Location.Y + 25), new Point(Part.Location.X + 100, Part.Location.Y + 25)));
                        break;
                    case Helper.Helper.Direction.North:
                        RectangleGeometry rectang = new RectangleGeometry(new Rect(Part.Location.X + 12.5, Part.Location.Y + 25, 25, 50));
                        gg.Children.Add(rectang);

                        gg.Children.Add(new LineGeometry(new Point(Part.Location.X + 25, Part.Location.Y), new Point(Part.Location.X + 25, Part.Location.Y + 25)));
                        gg.Children.Add(new LineGeometry(new Point(Part.Location.X + 25, Part.Location.Y + 75), new Point(Part.Location.X + 25, Part.Location.Y + 100)));
                        break;
                    default:
                        break;
                }

                p.Data = gg;
                
                return p;
            }
        }
    }
}