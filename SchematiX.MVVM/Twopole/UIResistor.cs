﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Controls;
using SchematiX.Helper;

namespace SchematiX.MVVM
{
    public class UIResistor : UIPart
    {
        public override Path Path
        {
            get
            {
                Path p = base.Path;
                GeometryGroup gg = new GeometryGroup();

                switch (Part.Direction)
                {
                    case Helper.Helper.Direction.Unspec:
                        break;
                    case Helper.Helper.Direction.West:
                    case Helper.Helper.Direction.East:
                        gg.Children.Add(new RectangleGeometry(new Rect(Part.Location.X + 10, Part.Location.Y,
                                    Part.Size.Width - 20, Part.Size.Height)));
                        gg.Children.Add(new LineGeometry(
                            Part.Plugs[0].Location,
                            new Point(Part.Location.X + 10, Part.Location.Y + Part.Size.Height / 2)));

                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Location.X + Part.Size.Width - 10, Part.Location.Y + Part.Size.Height / 2),
                            Part.Plugs[1].Location));                       
                        break;
                    case Helper.Helper.Direction.North:
                    case Helper.Helper.Direction.South:
                        gg.Children.Add(new RectangleGeometry(new Rect(Part.Location.X, Part.Location.Y + 10,
                                    Part.Size.Width, Part.Size.Height - 20)));
                        gg.Children.Add(new LineGeometry(
                           Part.Plugs[0].Location,
                           new Point(Part.Location.X + Part.Size.Width / 2, Part.Location.Y + 10)));

                        gg.Children.Add(new LineGeometry(
                            new Point(Part.Location.X + Part.Size.Width / 2, Part.Location.Y + Part.Size.Height - 10),
                            Part.Plugs[1].Location));
                        break;
                    default:
                        break;
                }


                //gg.Children.Add(new LineGeometry(Part.Junctions[0].Location, Part.Plugs[0].Location));
                //gg.Children.Add(new LineGeometry(Part.Plugs[1].Location, Part.Junctions[1].Location));
                p.Data = gg;
                return p;
            }
        }

        public UIResistor()
        {

        }
    }
}