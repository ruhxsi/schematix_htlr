using SchematiX.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SchematiX.MVVM
{
    public class UIVoltage : UIPart
    {
        public override Path Path

        {
            get
            {
                Path p = base.Path;
                GeometryGroup gg = new GeometryGroup();
                gg.FillRule = FillRule.Nonzero;

                switch (Part.Direction)
                {
                    case Helper.Helper.Direction.Unspec:
                        break;
                    case Helper.Helper.Direction.West:
                        gg.Children = new GeometryCollection
                {
                    new LineGeometry(new Point(Part.Location.X, Part.Location.Y + 25), new Point(Part.Location.X + 100, Part.Location.Y + 25)),
                    new EllipseGeometry(new Point(Part.Location.X + 50, Part.Location.Y + 25), 25, 25),

                    new LineGeometry(new Point(Part.Location.X + 25, Part.Location.Y - 30),new Point(Part.Location.X + 75,Part.Location.Y-30)),
                    new LineGeometry(new Point(Part.Location.X + 40 , Part.Location.Y - 35), new Point(Part.Location.X + 25 , Part.Location.Y-30)),
                    new LineGeometry(new Point(Part.Location.X + 40, Part.Location.Y - 25), new Point(Part.Location.X + 25,Part.Location.Y-30)),
                    
                };
                        break;
                    case Helper.Helper.Direction.East:
                        gg.Children = new GeometryCollection
                {
                    new LineGeometry(new Point(Part.Location.X, Part.Location.Y + 25), new Point(Part.Location.X + 100, Part.Location.Y + 25)),
                    new EllipseGeometry(new Point(Part.Location.X + 50, Part.Location.Y + 25), 25, 25),

                   new LineGeometry(new Point(Part.Location.X + 25, Part.Location.Y - 30),new Point(Part.Location.X + 75,Part.Location.Y-30)),
                    new LineGeometry(new Point(Location.X + 55 , Location.Y - 35), new Point(Location.X + 75, Location.Y-30)),
                    new LineGeometry(new Point(Location.X + 55, Location.Y - 25), new Point(Location.X + 75,Location.Y - 30)),
                    
                };
                        break;
                    case Helper.Helper.Direction.North:
                        gg.Children = new GeometryCollection
                    {
                       new LineGeometry(new Point(Part.Location.X+25, Part.Location.Y ), new Point(Part.Location.X +25, Part.Location.Y + 100)),
                        new EllipseGeometry(new Point(Part.Location.X +25, Part.Location.Y + 50), 25, 25),

                    new LineGeometry(new Point(Part.Location.X - 30, Part.Location.Y + 25),new Point(Part.Location.X - 30,Part.Location.Y+75)),
                    new LineGeometry(new Point(Part.Location.X -35 , Part.Location.Y + 40), new Point(Part.Location.X - 30 , Part.Location.Y+25)),
                    new LineGeometry(new Point(Part.Location.X - 25, Part.Location.Y + 40), new Point(Part.Location.X - 30,Part.Location.Y+25)),
                    
                    };
                        break;
                    case Helper.Helper.Direction.South:
                        gg.Children = new GeometryCollection
                    {
                       new LineGeometry(new Point(Part.Location.X+25, Part.Location.Y ), new Point(Part.Location.X +25, Part.Location.Y + 100)),
                        new EllipseGeometry(new Point(Part.Location.X +25, Part.Location.Y + 50), 25, 25),

                         new LineGeometry(new Point(Part.Location.X - 30, Part.Location.Y + 25),new Point(Part.Location.X - 30,Part.Location.Y+75)),
                        new LineGeometry(new Point(Part.Location.X -35 , Part.Location.Y + 60), new Point(Part.Location.X - 30 , Part.Location.Y+75)),
                        new LineGeometry(new Point(Part.Location.X - 25, Part.Location.Y + 60), new Point(Part.Location.X - 30,Part.Location.Y+75)),

                    };
                        break;
                    default:
                        break;
                }

                p.Data = gg;

                return p;
            }

        }
    }
}

