﻿using SchematiX.Model;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using static SchematiX.Helper.Helper;


namespace SchematiX.MVVM
{
    public class UIJunction : UIPart
    {
        public override Path Path
        {
            get
            {
                Path p = base.Path;
                p.Fill = p.Stroke;
                
                GeometryGroup gg = new GeometryGroup();

                foreach (var item in Part.Plugs)
                {
                    switch (item.Direction)
                    {
                        case Direction.Unspec:
                            gg.Children.Add(new LineGeometry(Location, item.Location)); // für location einen sinvollen wert geben, so was wie horrizontal zu einem plug
                            break;
                        case Direction.East:
                            gg.Children.Add(
                                new LineGeometry(
                                        new Point(Location.X, item.Location.Y),
                                        Location));
                            gg.Children.Add(
                                new LineGeometry(
                                    new Point(Location.X, item.Location.Y),
                                    item.Location));
                            break;
                        case Direction.North:
                            gg.Children.Add(
                                new LineGeometry(
                                    new Point(item.Location.X, Location.Y),
                                    item.Location));
                            gg.Children.Add(
                                new LineGeometry(
                                    new Point(item.Location.X, Location.Y),
                                    Location));
                            break;
                        case Direction.South:
                            gg.Children.Add(
                                new LineGeometry(
                                    new Point(item.Location.X, Location.Y),
                                    item.Location));
                            gg.Children.Add(
                                new LineGeometry(
                                    new Point(item.Location.X, Location.Y),
                                    Location));
                            break;
                        case Direction.West:
                            gg.Children.Add(
                                new LineGeometry(
                                     new Point(Location.X, item.Location.Y),
                                     Location));
                            gg.Children.Add(
                                new LineGeometry(
                                    new Point(Location.X, item.Location.Y),
                                    item.Location));
                            break;
                        default:
                            break;
                    }
                }

                if (Part.Plugs.Count > 2)
                {
                    gg.Children.Add(new EllipseGeometry(Location, 4, 4));
                }

                p.Data = gg;
                return p;
            }
        }


        public override void SetOffset(Vector offset)
        {
            base.SetOffset(offset);
        }

        public void AddConnection(Node plug)
        {
            if (Part is Junction)
            {
                ((Junction)Part).AddConnection(plug);
                OnPropertyChanged("Path");
                plug.PropertyChanged += Part_PropertyChanged;
            }
        }

        private void Part_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged("Path");
        }
    }
}
