﻿using System.Collections.Generic;

namespace SchematiX.Model
{
    public class Junction : SinglePole
    {
        public void AddConnection(Node plug)
        {
            Plugs.Add(plug);
        }

        public void RemoveConnection(Node plug)
        {
            Plugs.Remove(plug);
        }

        protected override void AdjustPlugs()
        {
            if (Plugs == null)
                Plugs = new List<Node>();
        }
    }
}
