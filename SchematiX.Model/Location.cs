﻿using System;
using System.Windows;

namespace SchematiX.Model
{
    public class Location : NotifyPropertyChangedBase
    {
        private double x;
        private double y;

        public double X
        {
            get { return x; }
            set
            {
                x = value;
                OnPropertyChanged("X");
            }
        }

        public double Y
        {
            get { return y; }
            set
            {
                y = value;
                OnPropertyChanged("Y");
            }
        }
        public Location(double x, double y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return $"{X:0.00} / {Y:0.00}";
        }

        public Point GetOffset(Vector vector)
        {
            return this + vector;
        }

        // implicit conversion operator
        public static implicit operator Point(Location location)
        {
            return new Point(location.X, location.Y);
        }

        public static implicit operator Location(Point point)
        {
            return new Location(point.X, point.Y);
        }

        public void SetOffset(Vector offset)
        {
            X += offset.X;
            Y += offset.Y;
        }

        public static Location operator +(Location left, Vector right)
        {
            return new Location(left.X + right.X, left.Y + right.Y);
        }
    }
}