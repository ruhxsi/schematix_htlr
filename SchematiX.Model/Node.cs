﻿using static SchematiX.Helper.Helper;

namespace SchematiX.Model
{
    public class Node : NotifyPropertyChangedBase
    {
        protected static uint index = 0;
        private Location location;
        private Direction direction;

        public Direction Direction
        {
            get { return direction; }
            set { direction = value; }
        }

        public string Name { get; set; }
        public Location Location {
            get
            {
                return location;
            }
            set
            {
                if(location != null)
                    location.PropertyChanged -= Location_PropertyChanged;
                if (value != null)
                    value.PropertyChanged += Location_PropertyChanged;
                location = value;
                OnPropertyChanged("Location");
            }
        }

        private void Location_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            OnPropertyChanged("Location");
        }

        public Node(Location location )
        {
            Location = location;
            Name = $"N_{index}";
            index++;
        }
    }
}
