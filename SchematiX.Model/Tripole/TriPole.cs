﻿using System.Collections.Generic;
using System.Windows;
using static SchematiX.Helper.Helper;

namespace SchematiX.Model
{
    public class TriPole : Part
    {
        public override Location Location
        {
            get => base.Location;
            set
            {
                base.Location = value;
                AdjustPlugs();
            }
        }

        protected override void AdjustPlugs()
        {
            Plugs = new List<Node>();

            switch (Direction)
            {
                case Direction.West:
                    Plugs.Add(new Node(new Location(Location.X, Location.Y + (Size.Height / 2))));
                    Plugs.Add(new Node(new Location(Location.X + Size.Width, Location.Y)));
                    Plugs.Add(new Node(new Location(Location.X + Size.Width, Location.Y + Size.Height)));
                    break;
                case Direction.North:
                    Plugs.Add(new Node(new Location(Location.X + (Size.Width / 2), Location.Y)));
                    Plugs.Add(new Node(new Location(Location.X + Size.Width, Location.Y + Size.Height)));
                    Plugs.Add(new Node(new Location(Location.X, Location.Y + Size.Height)));
                    break;
                case Direction.East:
                    Plugs.Add(new Node(new Location(Location.X + Size.Width, Location.Y + (Size.Height / 2))));
                    Plugs.Add(new Node(new Location(Location.X, Location.Y + Size.Height)));
                    Plugs.Add(new Node(new Location(Location.X, Location.Y)));
                    break;
                case Direction.South:
                    Plugs.Add(new Node(new Location(Location.X + (Size.Width / 2), Location.Y + Size.Height)));
                    Plugs.Add(new Node(new Location(Location.X, Location.Y)));
                    Plugs.Add(new Node(new Location(Location.X + Size.Width, Location.Y)));
                    break;
                default:
                    break;
            }
        }

        public override Size Size
        {
            get => base.Size;
            set
            {
                base.Size = value;
                AdjustPlugs();
            }
        }

        public TriPole() : base()
        {

        }
    }
}
