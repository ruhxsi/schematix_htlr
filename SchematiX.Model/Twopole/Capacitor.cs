﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchematiX.Model
{
    public class Capacitor : TwoPole
    {
        public double Tolerance { get; set; }

        public Capacitor() : base()
        {
            Name = $"C_{index}";
        }
    }
}
