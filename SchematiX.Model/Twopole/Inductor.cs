﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchematiX.Model
{
    public class Inductor : TwoPole
    {
        public double Tolerance { get; set; }
        
        public Inductor() : base()
        {
            Name = $"L_{index}";
        }
    }
}
